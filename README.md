# Angular Table

The objective is to implement a TABLE component according to the specifications outlined below, using the listed technologies only – please do not use 3rd party table components such as those included with Bootstrap, Angular Material, etc.

## Get started

### Install npm packages

Install the `npm` packages described in the `package.json` and verify that it works:

```shell
npm install
ng serve
```

#### npm scripts

These are the most useful commands defined in `package.json`:

* `ng lint` - to ensure linting standards are adhered to (standard tslintrules from Angular CLI)
* `ng test` - run ng test to ensure all unit tests pass
* `ng build` - run ng build to create the dist version of the app

